create table Employee (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([Id] ASC)
)
go

create table Salary (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[DT] [Datetime] NOT NULL,
	[Summ] [float] NOT NULL,
	CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED ([Id] ASC)
)
go

ALTER TABLE Salary  WITH CHECK ADD CONSTRAINT [FK_Salary_Employee] 
	FOREIGN KEY([EmployeeId])
	REFERENCES Employee ([Id])
GO

ALTER TABLE Salary CHECK CONSTRAINT [FK_Salary_Employee]
GO