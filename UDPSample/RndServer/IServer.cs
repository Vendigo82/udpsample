﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RndServer {
    /// <summary>
    /// Server interface
    /// </summary>
    public interface IServer {
        /// <summary>
        /// starting work
        /// </summary>
        void start(IEnumerable<int> digits);
    }
}
