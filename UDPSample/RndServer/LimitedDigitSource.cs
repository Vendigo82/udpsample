﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RndServer {
    /// <summary>
    /// Generate specific count digits
    /// </summary>
    public class LimitedDigitSource : IEnumerable<int> {
        private readonly IDigitGenerator _gen;
        private readonly int _count;

        public LimitedDigitSource(IDigitGenerator gen, int count) {
            _gen = gen;
            _count = count;
        }

        public IEnumerator<int> GetEnumerator() {
            for (int i = 0; i < _count; ++i)
                yield return _gen.next();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return this.GetEnumerator();
        }
    }
}
