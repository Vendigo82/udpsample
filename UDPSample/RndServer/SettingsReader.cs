﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Settings;

namespace RndServer {
    public class SettingsReader : Settings.SettingsReader<ServerSettings> {
        protected override void load(XElement root, ServerSettings s) {
            base.load(root, s);
            s.minValue = int.Parse(root.Element("minValue").Value);
            s.maxValue = int.Parse(root.Element("maxValue").Value);
        }
    }
}
