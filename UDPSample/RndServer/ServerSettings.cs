﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RndServer {
    public class ServerSettings : Settings.Settings {
        public int minValue { get; set; }
        public int maxValue { get; set; }
    }
}
