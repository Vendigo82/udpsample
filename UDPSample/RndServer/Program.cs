﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RndServer {
    class Program {
        static void Main(string[] args) {
            //read setting
            ServerSettings s;
            try {
                s = new SettingsReader().loadSettings("cfg.xml");
            } catch (Exception e) {
                Console.WriteLine("Error load settings: " + e.Message);
                return;
            }

            //run send packets server
            IDigitGenerator gen = new RandomDigitGenerator(s.minValue, s.maxValue);
            //IDigitGenerator gen = new IncrementalDigitGenerator();
            SimpleUDPServer server = new SimpleUDPServer(s.address, s.port);
            while (true) {
                try {
                    server.start(new InfinityDigitSource(gen));
                    //server.start(new LimitedDigitSource(gen, 1000));
                    return;
                } catch (Exception e) {
                    //if exception occurs, show exception and restart sending
                    //real program must see, what exception occurs and try do something for fix it
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }
}
