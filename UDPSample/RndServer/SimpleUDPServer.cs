﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

namespace RndServer {
    public class SimpleUDPServer : IServer {
        private readonly string _address;
        private readonly int _port;
        //number of packet
        private UInt64 _number = 0;

        //single packet size, include package number and digit
        private const int PACKET_SIZE = sizeof(UInt64) + sizeof(int);

        public SimpleUDPServer(string multicastAddress, int port) {
            _address = multicastAddress;
            _port = port;
        }

        /// <summary>
        /// start sending
        /// can throw exception, see UdpClient.Send for more information
        /// </summary>
        /// <param name="digits">Digit sequence for sending</param>        
        public void start(IEnumerable<int> digits) {         
            using (UdpClient client = new UdpClient()) {
                IPAddress ma = IPAddress.Parse(_address);
                client.JoinMulticastGroup(ma);
                IPEndPoint remote = new IPEndPoint(ma, _port);

                byte[] buffer = new byte[PACKET_SIZE];
                foreach (int digit in digits) {
                    preparePacket(buffer, _number, digit);
                    client.Send(buffer, PACKET_SIZE, remote);
                    ++_number;
                }
            }
        }        

        private void preparePacket(byte[] buffer, UInt64 no, int digit) {
            byte[] noBuffer = BitConverter.GetBytes(no);
            byte[] digitBuffer = BitConverter.GetBytes(digit);
            Buffer.BlockCopy(noBuffer, 0, buffer, 0, noBuffer.Length);
            Buffer.BlockCopy(digitBuffer, 0, buffer, noBuffer.Length, digitBuffer.Length);
            //Array.Copy(noBuffer, 0, buffer, 0, noBuffer.Length);
            //Array.Copy(digitBuffer, 0, buffer, noBuffer.Length, digitBuffer.Length);
        }
    }
}
