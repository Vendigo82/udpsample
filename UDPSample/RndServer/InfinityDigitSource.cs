﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RndServer {
    /// <summary>
    /// Endless digit generator
    /// </summary>
    public class InfinityDigitSource : IEnumerable<int> {
        private readonly IDigitGenerator _gen;

        public InfinityDigitSource(IDigitGenerator gen) {
            _gen = gen;
        }

        public IEnumerator<int> GetEnumerator() {
            while (true)
                yield return _gen.next();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return this.GetEnumerator();
        }
    }
}
