﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RndServer {
    /// <summary>
    /// generate random digit in diapasone [a,b)
    /// </summary>
    public class RandomDigitGenerator : IDigitGenerator {
        private readonly int _minValue;
        private readonly int _maxValue;
        private readonly Random _rnd = new Random();

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="minValue">low range of diapason</param>
        /// <param name="maxValue">hight range of diapasone</param>
        /// <exception cref="ArgumentException">throws if maxValue <= minValue</exception>
        public RandomDigitGenerator(int minValue, int maxValue) {
            if (maxValue <= minValue)
                throw new ArgumentException("invalid diapasone, b must be great then a");

            _minValue = minValue;
            _maxValue = maxValue;
        }

        /// <summary>
        /// generate next digit
        /// </summary>
        /// <returns>new digit</returns>
        public int next() {
            return _rnd.Next(_minValue, _maxValue);
        }
    }
}
