﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RndServer {
    /// <summary>
    /// interface for generate digits
    /// </summary>
    public interface IDigitGenerator {
        /// <summary>
        /// generate next digit
        /// </summary>
        /// <returns>new digit</returns>
        int next();
    }
}
