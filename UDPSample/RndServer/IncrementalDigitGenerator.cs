﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RndServer {
    /// <summary>
    /// Generate incremental sequence
    /// </summary>
    public class IncrementalDigitGenerator : IDigitGenerator {
        private int _current = 0;

        public int next() {
            return _current++;
        }
    }
}
