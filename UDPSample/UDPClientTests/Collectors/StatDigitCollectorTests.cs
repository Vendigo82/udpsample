﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UDPClient.Collectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient.Collectors.Tests {
    [TestClass()]
    public class StatDigitCollectorTests {
        const double delta = 0.001;

        [TestMethod()]
        public void StatDigitTest1() {
            StatDigitCollector collector = new StatDigitCollector();
            collector.accept(0);
            collector.accept(0);
            collector.accept(14);
            collector.accept(14);

            double avg = collector.avg();
            Assert.AreEqual(7, avg, delta);
            Assert.AreEqual(7, collector.deviation(avg), delta);
        }

        [TestMethod()]
        public void StatDigitTest2() {
            StatDigitCollector collector = new StatDigitCollector();
            collector.accept(0);
            collector.accept(6);
            collector.accept(8);
            collector.accept(14);

            double avg = collector.avg();
            Assert.AreEqual(7, avg, delta);
            Assert.AreEqual(5, collector.deviation(avg), delta);
        }

        [TestMethod()]
        public void StatDigitTest3() {
            StatDigitCollector collector = new StatDigitCollector();
            collector.accept(6);
            collector.accept(6);
            collector.accept(8);
            collector.accept(8);

            double avg = collector.avg();
            Assert.AreEqual(7, avg, delta);
            Assert.AreEqual(1, collector.deviation(avg), delta);
        }

        [TestMethod()]
        public void StatDigitMedianaTest1() {
            StatDigitCollector collector = new StatDigitCollector();
            collector.accept(6);
            collector.accept(8);
            collector.accept(3);
            collector.accept(3);
            collector.accept(7);
            collector.accept(9);
            collector.accept(1);

            Assert.AreEqual(6, collector.mediana(), delta);
            Assert.AreEqual(3, collector.moda());
        }

        [TestMethod()]
        public void StatDigitMedianaTest2() {
            StatDigitCollector collector = new StatDigitCollector();
            collector.accept(9);
            collector.accept(1);
            collector.accept(6);
            collector.accept(2);
            collector.accept(4);
            collector.accept(5);
            collector.accept(3);
            collector.accept(8);

            Assert.AreEqual(4.5, collector.mediana(), delta);
            Assert.AreEqual(9, collector.moda());
        }

        [TestMethod()]
        public void StatDigitMedianaTest3() {
            StatDigitCollector collector = new StatDigitCollector();
            collector.accept(2);
            collector.accept(1);
            collector.accept(14);
            collector.accept(2);
            collector.accept(3);
            collector.accept(2);

            Assert.AreEqual(2, collector.mediana(), delta);
            Assert.AreEqual(2, collector.moda());
        }
    }
}