﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UDPClient.Collectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient.Collectors.Tests {
    [TestClass()]
    public class StatPacketCollectorTests {
        [TestMethod()]
        public void acceptTest() {
            StatPacketCollector collector = new StatPacketCollector();
            collector.accept(0, 0);
            Assert.AreEqual<UInt64>(1, collector.recievedCount);
            Assert.AreEqual<UInt64>(0, collector.lostCount);

            for (UInt64 i = 1; i < 10; ++i)
                collector.accept(i, 0);
            Assert.AreEqual<UInt64>(10, collector.recievedCount);
            Assert.AreEqual<UInt64>(0, collector.lostCount);

            collector.accept(19, 0);
            Assert.AreEqual<UInt64>(11, collector.recievedCount);
            Assert.AreEqual<UInt64>(9, collector.lostCount);

            collector.accept(18, 0);
            Assert.AreEqual<UInt64>(12, collector.recievedCount);
            Assert.AreEqual<UInt64>(8, collector.lostCount);

            collector.accept(15, 0);
            Assert.AreEqual<UInt64>(13, collector.recievedCount);
            Assert.AreEqual<UInt64>(7, collector.lostCount);

            for (UInt64 i = 20; i < 1000; ++i)
                collector.accept(i, 0);
            Assert.AreEqual<UInt64>(993, collector.recievedCount);
            Assert.AreEqual<UInt64>(7, collector.lostCount);

            collector.accept(0, 0);
            Assert.AreEqual<UInt64>(994, collector.recievedCount);
            Assert.AreEqual<UInt64>(7, collector.lostCount);
        }

        [TestMethod()]
        public void acceptTest2() {
            StatPacketCollector collector = new StatPacketCollector();
            for (UInt64 i = 10; i < 20; ++i)
                collector.accept(i, 0);
            Assert.AreEqual<UInt64>(10, collector.recievedCount);
            Assert.AreEqual<UInt64>(0, collector.lostCount);

            collector.accept(25, 0);
            Assert.AreEqual<UInt64>(11, collector.recievedCount);
            Assert.AreEqual<UInt64>(5, collector.lostCount);
        }

        [TestMethod()]
        public void acceptTest3() {
            StatPacketCollector collector = new StatPacketCollector();
            for (UInt64 i = 10; i < 20; ++i)
                collector.accept(i, 0);
            Assert.AreEqual<UInt64>(10, collector.recievedCount);
            Assert.AreEqual<UInt64>(0, collector.lostCount);

            //duplicate number
            collector.accept(15, 0);
            Assert.AreEqual<UInt64>(11, collector.recievedCount);
            Assert.AreEqual<UInt64>(0, collector.lostCount);
        }
    }
}