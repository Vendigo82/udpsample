﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

namespace UDPClient {
    public class SimpleUDPClient : IClient {
        //started listening or not
        private bool _started = false;
        //if canceled, then false
        private bool _running = false;

        private readonly string _address;
        private readonly int _port;
        private readonly IPacketCollector _collector;
        private int _bufferSize = 0;

        private const int OFFSET = sizeof(UInt64);
        //single packet size, include package number and digit
        private const int PACKET_SIZE = sizeof(UInt64) + sizeof(int);

        /// <summary>
        /// call when receive invalid packet
        /// 1 param: sender, this object
        /// 2 param: buffer
        /// 3 param: count of received bytes
        /// </summary>
        public event Action<SimpleUDPClient, byte[], int> onInvalidPacket;
        /// <summary>
        /// call when SocketException was occured
        /// 1 param: sender, this object
        /// 2 param: buffer
        /// 3 param: exception
        /// </summary>
        public event Action<SimpleUDPClient, byte[], SocketException> onSocketException;

        public SimpleUDPClient(string multicastAddress, int port, IPacketCollector collector) {
            _address = multicastAddress;
            _port = port;
            _collector = collector;
        }

        public int socketBufferSize { get { return _bufferSize; } set { _bufferSize = value; } }

        public void start() {
            if (_started)
                throw new ApplicationException("Already started");
            _running = true;
            _started = true;

            using (UdpClient client = new UdpClient(_port)) {
                //IPEndPoint localep = new IPEndPoint(IPAddress.Any, _port);
                //client.Client.Bind(localep);

                //for force lost data
                if (_bufferSize > 0)
                    client.Client.ReceiveBufferSize = _bufferSize;

                //join to UDP multicast group
                IPAddress ma = IPAddress.Parse(_address);
                client.JoinMulticastGroup(ma);

                try {
                    routine(client);
                } finally {
                    _started = false;
                }
            }
        }

        public void stop() {
            _running = false;
        }

        private void routine(UdpClient client) {
            IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);
            EndPoint remoteep = (EndPoint)remote;

            byte[] buffer = new byte[PACKET_SIZE];
            while (_running) {
                try {
                    //read bytes
                    int count = client.Client.ReceiveFrom(buffer, 0, PACKET_SIZE, SocketFlags.None, ref remoteep);
                    //byte[] buffer = client.Receive(ref remote);

                    //check packet size
                    if (count == PACKET_SIZE) {
                        //accept this packet
                        UInt64 no = BitConverter.ToUInt64(buffer, 0);
                        int digit = BitConverter.ToInt32(buffer, OFFSET);
                        _collector.accept(no, digit);
                    }
                    else {
                        //report about invalid packet
                        onInvalidPacket?.Invoke(this, buffer, count);
                    }
                }
                catch (SocketException e) {
                    //report about exception
                    onSocketException?.Invoke(this, buffer, e);
                }
            }
        }
    }
}
