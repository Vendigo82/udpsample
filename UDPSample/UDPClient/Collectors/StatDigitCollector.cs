﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient.Collectors {
    /// <summary>
    /// Count digit statistic
    /// </summary>
    public class StatDigitCollector : IDigitCollector {
        //collected digits. Key - digit value, Value - count of digits
        private Dictionary<int, uint> _values = new Dictionary<int, uint>();
        //ordered array for mediana
        private SortedSet<int> _sorted = new SortedSet<int>();
        //total count of accepted values
        private UInt64 _count = 0;

        public void accept(int digit) {
            uint cnt;
            if (!_values.TryGetValue(digit, out cnt)) {
                cnt = 0;
                _sorted.Add(digit);
            }
            _values[digit] = cnt + 1;

            ++_count;
        }

        /// <summary>
        /// returns true, is have not accepted values
        /// </summary>
        public bool isEmpty { get { return _values.Count == 0; } }

        /// <summary>
        /// average value
        /// </summary>
        public double avg() {
            Int64 summ = 0;
            Int64 count = 0;
            foreach (KeyValuePair<int, uint> item in _values) {
                summ += item.Key * item.Value;
                count += item.Value;
            }

            return summ / (double)count;
        }

        /// <summary>
        /// standard deviation
        /// </summary>
        public double deviation(double avg) {
            double summ = 0;
            Int64 count = 0;
            foreach (KeyValuePair<int, uint> item in _values) {
                summ += (item.Key - avg) * (item.Key - avg) * item.Value;
                count += item.Value;
            }

            return Math.Sqrt(summ / count);
        }

        /// <summary>
        /// Most frequency value
        /// </summary>
        public int moda() {
            int value = 0;
            uint max = 0;
            foreach (KeyValuePair<int, uint> pair in _values) {
                if (pair.Value > max) {
                    value = pair.Key;
                    max = pair.Value;
                }
            }
            return value;
        }

        /// <summary>
        /// mediana
        /// </summary>
        public double mediana() {
            if (isEmpty)
                return 0;

            //int[] array = _values.Keys.ToArray();
            //Array.Sort(array);

            bool useAvg = false;
            UInt64 halfIndex = _count / 2;
            if (halfIndex * 2 != _count)
                halfIndex += 1;
            else
                useAvg = true;

            UInt64 currentIndex = 0;
            IEnumerator<int> iter = _sorted.GetEnumerator();
            while (iter.MoveNext()) { 
                //for (int i = 0; i < array.Length; ++i) {
                //currentIndex += _values[array[i]];
                currentIndex += _values[iter.Current];
                if (currentIndex == halfIndex && useAvg) {
                    int first = iter.Current;
                    iter.MoveNext();
                    return (first + iter.Current) / 2.0;
                    //return (array[iter.] + array[i + 1]) / 2.0;
                }
                if (currentIndex >= halfIndex)
                    return iter.Current;
                    //return array[i];
            }

            return _sorted.Last();
        }
    }
}
