﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace UDPClient.Collectors {
    /// <summary>
    /// perform Thread.Sleep after receive packet
    /// </summary>
    public class DelayPacketCollector : IPacketCollector {
        private readonly IPacketCollector _collector;
        private readonly int _delay;

        public DelayPacketCollector(IPacketCollector collector, int delay) {
            _collector = collector;
            _delay = delay;
        }

        public void accept(ulong packetNumber, int digit) {
            _collector.accept(packetNumber, digit);
            Thread.Sleep(_delay);
        }
    }
}
