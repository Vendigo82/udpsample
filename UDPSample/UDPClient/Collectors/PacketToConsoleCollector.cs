﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient.Collectors {
    /// <summary>
    /// Write packet to console, and transfer data to another PacketCollector
    /// </summary>
    public class PacketToConsoleCollector : IPacketCollector {
        private readonly IPacketCollector _collector;

        public PacketToConsoleCollector() {
            _collector = null;
        }

        public PacketToConsoleCollector(IPacketCollector collector) {
            _collector = collector;
        }

        public void accept(ulong packetNumber, int digit) {
            Console.WriteLine("{0}: {1}", packetNumber, digit);
            if (_collector != null)
                _collector.accept(packetNumber, digit);
        }
    }
}
