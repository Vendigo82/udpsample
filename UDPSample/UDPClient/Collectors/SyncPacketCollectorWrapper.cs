﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UDPClient.Collectors {
    /// <summary>
    /// Add to Collector thread-safety access
    /// </summary>
    /// <typeparam name="T">Collector type</typeparam>
    public class SyncPacketCollectorWrapper<T> : IPacketCollector where T : IPacketCollector {
        //collector
        private readonly T _collector;
        //use for Monitor
        private readonly object _locker = new object();

        private struct Packet {
            public UInt64 no;
            public int digit;
        }

        //buffer for accepted data, use if Monitor is busy
        private List<Packet> _buffer = new List<Packet>(20);
        //maximum buffer size
        private uint _maxBufferSize = 0;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="collector">collector for wrap</param>
        public SyncPacketCollectorWrapper(T collector) {
            _collector = collector;
        }        

        public void accept(UInt64 packetNumber, int digit) {
            //try enter to critical section
            if (Monitor.TryEnter(_locker)) {
                try {
                    //check buffer exists items
                    if (_buffer.Count != 0) {
                        for (int i = 0; i < _buffer.Count; ++i)
                            _collector.accept(_buffer[i].no, _buffer[i].digit);
                        _buffer.Clear();
                    }

                    //add last item
                    _collector.accept(packetNumber, digit);
                } finally {
                    //exit from ctritical section
                    Monitor.Exit(_locker);
                }
            } else {
                //critical section is owned by other thread, add values to buffer
                _buffer.Add(new Packet { no = packetNumber, digit = digit });
                if (_buffer.Count > _maxBufferSize)
                    _maxBufferSize = (uint)_buffer.Count;
            }
        }

        /// <summary>
        /// Thread-safety access to Collector
        /// Don't foget call Dispose
        /// </summary>
        public IHandler getCollector() {
            Monitor.Enter(_locker);
            return new Handler(this);
        }

        public uint maxBufferSize { get { return _maxBufferSize; } }

        /// <summary>
        /// release monitor
        /// </summary>
        private void releaseCollector() {
            Monitor.Exit(_locker);
        }

        /// <summary>
        /// Public interface for access to collector
        /// </summary>
        public interface IHandler : IDisposable {
            T value { get; }
        }

        /// <summary>
        /// private class, perform access to collector
        /// </summary>
        private class Handler : IHandler {
            private readonly SyncPacketCollectorWrapper<T> _parent;

            public Handler(SyncPacketCollectorWrapper<T> parent) {
                _parent = parent;
            }

            public T value { get { return _parent._collector; } }

            public void Dispose() {
                _parent.releaseCollector();
            }
        }
    }
}
