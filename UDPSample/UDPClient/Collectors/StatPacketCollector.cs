﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient.Collectors {
    /// <summary>
    /// Count statistic for packets
    /// </summary>
    public class StatPacketCollector : StatDigitCollector, IPacketCollector {
        //first received packet number
        private UInt64 _firstNumber = UInt64.MaxValue;
        //last received packet number
        private UInt64 _lastNumber = 0;
        //count of received packets
        private UInt64 _countPackets = 0;
        //total count of received packets
        private UInt64 _countPacketsTotal = 0;
        //total count of lost packets
        private UInt64 _countLostTotal = 0;

        public void accept(ulong packetNumber, int digit) {
            if (_countPackets == 0) {
                //first packet in row
                _firstNumber = packetNumber;
                _lastNumber = packetNumber;
                _countPackets += 1;
            } else {
                if (packetNumber + 100 < _lastNumber) {
                    _countPacketsTotal += _countPackets;
                    _countLostTotal = _lastNumber + 1 - _firstNumber - _countPackets;
                    _countPackets = 0;
                    _firstNumber = packetNumber;
                    _lastNumber = packetNumber;
                }

                //use max and min, because order not guarantied
                _firstNumber = Math.Min(packetNumber, _firstNumber);
                _lastNumber = Math.Max(packetNumber, _lastNumber);
                ++_countPackets;
            }

            accept(digit);
        }

        /// <summary>
        /// count of packets was recieved
        /// </summary>
        public UInt64 recievedCount { get { return _countPackets + _countPacketsTotal; } }

        /// <summary>
        /// count of lost packets
        /// </summary>
        public UInt64 lostCount {
            get {
                if (_countPackets == 0)
                    return _countLostTotal;
                UInt64 range = _lastNumber + 1 - _firstNumber;
                if (range < _countPackets)
                    return _countLostTotal;
                else
                    return range - _countPackets + _countLostTotal;
            }
        }
    }
}
