﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient {
    public class ClientSettings : Settings.Settings {
        public int delay { get; set; }
        public int socketBufferSize { get; set; }
    }
}
