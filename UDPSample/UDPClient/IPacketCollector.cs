﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient {
    /// <summary>
    /// Collect received packets
    /// </summary>
    public interface IPacketCollector {
        /// <summary>
        /// collect single packet
        /// </summary>
        /// <param name="packetNumber">packet number</param>
        /// <param name="digit">digit</param>
        void accept(UInt64 packetNumber, int digit);
    }
}
