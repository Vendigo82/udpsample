﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Settings;

namespace UDPClient {
    public class SettingsReader : Settings.SettingsReader<ClientSettings> {
        protected override void load(XElement root, ClientSettings s) {
            base.load(root, s);
            s.delay = int.Parse(getValue(root.Element("delay"), "0"));
            s.socketBufferSize = int.Parse(getValue(root.Element("socketBufferSize"), "0"));
        }
    }
}
