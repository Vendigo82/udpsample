﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UDPClient.Collectors;
using System.Net.Sockets;

namespace UDPClient {
    class Program {
        static void Main(string[] args) {
            //load settings
            ClientSettings s;
            try {
                s = new SettingsReader().loadSettings("cfg.xml");
            }
            catch (Exception e) {
                Console.WriteLine("Error load settings: " + e.Message);
                return;
            }

            //prepare collector
            SyncPacketCollectorWrapper<StatPacketCollector> sync =
                new SyncPacketCollectorWrapper<StatPacketCollector>(new StatPacketCollector());
            IPacketCollector collector = sync;
            //add write to console received packets
            //collector = new PacketToConsoleCollector(collector);
            if (s.delay > 0) {
                Console.WriteLine("add Thread.Sleep: {0} ms", s.delay);
                collector = new DelayPacketCollector(collector, s.delay);
            }

            //prepare udp listener
            SimpleUDPClient client = new SimpleUDPClient(s.address, s.port, collector);
            client.onInvalidPacket += onInvalidPacket;
            client.onSocketException += onSocketException;
            if (s.socketBufferSize != 0) {
                Console.WriteLine("Set socket buffer size: {0} bytes", s.socketBufferSize);
                client.socketBufferSize = s.socketBufferSize;
            }

            //run listener in new task
            Task task = Task.Run(() => {
                client.start();
            });

            //show stats
            Console.WriteLine("Press ENTER for stats");
            while (true) {
                ConsoleKeyInfo key = Console.ReadKey();
                if (key.Key == ConsoleKey.Enter) {
                    using (SyncPacketCollectorWrapper<StatPacketCollector>.IHandler handler = sync.getCollector()) {
                        Console.WriteLine("max buffer size: " + sync.maxBufferSize);
                        showStat(handler.value);
                    }
                }
            }
        }

        private static void onSocketException(SimpleUDPClient sender, byte[] buffer, SocketException e) {
            Console.WriteLine("exception: " + e.ToString() );
        }

        private static void onInvalidPacket(SimpleUDPClient sender, byte[] buffer, int count) {
            Console.WriteLine("Invalid packet");
        }

        static void showStat(StatPacketCollector stat) {
            Console.WriteLine("Total received: " + stat.recievedCount);
            Console.WriteLine("Lost: " + stat.lostCount);
            if (!stat.isEmpty) {
                double avg = stat.avg();
                Console.WriteLine("Avg: " + avg);
                Console.WriteLine("Deviation: " + stat.deviation(avg));
                Console.WriteLine("Moda: " + stat.moda());
                Console.WriteLine("Mediana: " + stat.mediana());
            }
            Console.WriteLine("-----------------");
        }
    }
}
