﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient {
    /// <summary>
    /// Client interface
    /// </summary>
    public interface IClient {
        /// <summary>
        /// start listening
        /// </summary>
        void start();

        /// <summary>
        /// stop listening
        /// </summary>
        void stop();
    }
}
