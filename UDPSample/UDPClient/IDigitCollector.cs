﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient {
    /// <summary>
    /// Collect digits
    /// </summary>
    public interface IDigitCollector {
        /// <summary>
        /// Accept single digit
        /// </summary>
        /// <param name="digit"></param>
        void accept(int digit);
    }
}
