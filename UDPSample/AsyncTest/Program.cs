﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncTest {
    class Program {

        static async Task performWrap() {
            Console.WriteLine("Before call perform, thread id " + Thread.CurrentThread.ManagedThreadId);
            await perform();
            Console.WriteLine("After call perform, thread id " + Thread.CurrentThread.ManagedThreadId);
        }

        static async Task perform() {
            Console.WriteLine("Before for, thread id " + Thread.CurrentThread.ManagedThreadId);
            for (int i = 0; i < 10000; ++i) {

            }
            Console.WriteLine("After for, thread id " + Thread.CurrentThread.ManagedThreadId);
        }

        static void Main(string[] args) {
            Console.WriteLine("Start, thread id " + Thread.CurrentThread.ManagedThreadId);
            Task t = performWrap();
            Console.WriteLine("After perform, thread id " + Thread.CurrentThread.ManagedThreadId);
            Console.ReadKey();
        }
    }
}
