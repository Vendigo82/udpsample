﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Settings {
    public class SettingsReader<T> where T : Settings, new() {
        public T loadSettings(string fn) {
            T s = new T();
            XElement root = XElement.Load(fn);
            load(root, s);
            return s;
        }

        protected virtual void load(XElement root, T s) {
            s.address = root.Element("address").Value;
            s.port = int.Parse(root.Element("port").Value);
        }

        protected string getValue(XElement e, string def) {
            if (e == null || e.Value == null)
                return def;
            else
                return e.Value;
        }
    }
}
